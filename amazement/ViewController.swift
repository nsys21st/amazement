//
//  ViewController.swift
//  amazement
//
//  Created by Arief Shaifullah Akbar on 09/05/19.
//  Copyright © 2019 Arief Shaifullah Akbar. All rights reserved.
//

import UIKit
import Lottie
import AVFoundation

class ViewController: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet var reactionView: AnimationView!
    @IBOutlet var backgroundView: AnimationView!
    @IBOutlet weak var confettiView: AnimationView!
    
    let reactionAnimation = Animation.named("wow")
    let bgAnimation = Animation.named("gradientBG")
    let confettiAnimation = Animation.named("fireworks")
    
    var audioPlayer: AVAudioPlayer!
    
    @IBOutlet weak var resetButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var style:UIStatusBarStyle = .default
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        freezeAnimation()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.addPulse))
        
        tapGestureRecognizer.numberOfTapsRequired = 1
        view.addGestureRecognizer(tapGestureRecognizer)
    }

    // animation will play after shaking phone
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        playAnimation()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch! = (touches.first)
        let location = touch.location(in: self.view)
        confettiView.center = location
        
        if location.x < 414 / 2 {
            if reactionView.isAnimationPlaying == false {
                changeBackground()
                reactLeft()
                explode()
            } else {
                explode()
            }
        } else if location.x > 414 / 2 {
            if reactionView.isAnimationPlaying == false {
                changeBackground()
                reactRight()
                explode()
            } else {
                explode()
            }
        }
        
    }
    
//    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch: UITouch! = (touches.first)
//        let location = touch.location(in: self.view)
//        confettiView.center = location
//    }
    
    @objc func addPulse(touch: UITapGestureRecognizer) {
        
        let location = touch.location(in: self.view)
        let pulse = Pulsing(numberOfPulses: 1, radius: 110, position: location)
        pulse.animationDuration = 0.8
        pulse.backgroundColor = UIColor.white.cgColor
        self.view.layer.addSublayer(pulse)
        
    }
    
    @IBAction func resetButtonAction(_ sender: UIButton) {
        freezeAnimation()
    }
    
    func reactLeft() {

        reactionView.animation = reactionAnimation
        reactionView.play(fromFrame: 0, toFrame: 87, loopMode: LottieLoopMode.playOnce)
        reactionView.animationSpeed = 0.7
    }
    
    func reactRight() {
        reactionView.animation = reactionAnimation
        reactionView.play(fromFrame: 60, toFrame: 0, loopMode: LottieLoopMode.playOnce)
        reactionView.animationSpeed = 0.7
        
    }
    
    func explode() {
        confettiView.animation = confettiAnimation
        confettiView.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.playOnce)
        confettiView.animationSpeed = 1.5
        playSound(soundFileName: "fireworks_blast")

    }
    
    func changeBackground() {
        backgroundView.animation = bgAnimation
        backgroundView.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.autoReverse)
        backgroundView.animationSpeed = 3
    }
    
    func playAnimation() {

        reactionView.animation = reactionAnimation
        backgroundView.animation = bgAnimation
        confettiView.animation = confettiAnimation

        reactionView.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.playOnce)
        reactionView.animationSpeed = 0.7

        backgroundView.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.playOnce)
        backgroundView.animationSpeed = 2


        confettiView.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.playOnce)
        confettiView.animationSpeed = 1.5
        playSound(soundFileName: "fireworks_blast")
    }
    
    func freezeAnimation() {
        reactionView.animation = reactionAnimation
        backgroundView.animation = bgAnimation
        confettiView.animation = confettiAnimation

        reactionView.stop()
        backgroundView.stop()
        confettiView.stop()
    }
    
    func playSound(soundFileName: String) {
        let soundURL = Bundle.main.url(forResource: soundFileName, withExtension: "wav")
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: soundURL!)
        } catch {
            print(error)
        }
        
        audioPlayer.play()
    }
    
}
